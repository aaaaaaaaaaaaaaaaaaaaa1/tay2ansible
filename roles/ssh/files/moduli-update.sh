bash awk '$5 > 2000' /etc/ssh/moduli > "${HOME}/moduli" ;
wc -l "${HOME}/moduli" # make sure there is something left ;
mv "${HOME}/moduli" /etc/ssh/moduli ;
# If it does not exist, create it:

ssh-keygen -G "${HOME}/moduli" -b 4096 ;
ssh-keygen -T /etc/ssh/moduli -f "${HOME}/moduli" ;
rm "${HOME}/moduli"
echo "moduli re-built"
